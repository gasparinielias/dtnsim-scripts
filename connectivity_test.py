'''
Usage:
   connectivity_test.py <contact_plan_file> <num_nodes> [options]

Options:
    -k --hops <max_hops>    Max hops [default: num_nodes]
    -h --help               Show this screen
'''
from docopt import docopt

import sys
from collections import defaultdict

def parse_CP(cp_file):
    edges = defaultdict(lambda: defaultdict(list))
    with open(cp_file, 'r') as fp:
        for l in fp.readlines():
            if l[0] == '#' or l.isspace():
                continue

            _, u, t1, t2, n1, n2, _ = l.split()

            if u == "contact":
                edges[int(n1)][int(n2)].append((int(t1), int(t2)))

    return edges

def connectivity(num_nodes, node, edges, max_hops):
    MAX_DIST = 1e10
    dist = {}
    for i in range(1, num_nodes + 1):
        dist[i] = MAX_DIST
    dist[node] = 0

    for _ in range(max_hops):
        for x in edges.keys():
            for y in edges[x].keys():
                for (t1, t2) in edges[x][y]:
                    node_cost = max(dist[x], t1)
                    if node_cost < dist[y] and node_cost < t2:
                        dist[y] = node_cost

    reached_nodes = sum(1 for v in dist.values() if v != MAX_DIST) - 1
    return reached_nodes

def max_edges_between_nodes(edges):
    res = 0
    mx, my = None, None
    for x in edges.keys():
        for y in edges[x].keys():
            if res < len(edges[x][y]):
                res = len(edges[x][y])
                mx = x
                my = y

    return res, mx, my

def nodes_with_direct_contact(edges, node_from):
    connected_nodes = []
    for x in edges.keys():
        for y in edges[x].keys():
            if x == node_from:
                if y not in connected_nodes:
                    connected_nodes.append(y)

            if y == node_from:
                if x not in connected_nodes:
                    connected_nodes.append(x)

    return sorted(connected_nodes)


def main():
    args = docopt(__doc__)
    cp_file = args["<contact_plan_file>"]
    nodes = int(args["<num_nodes>"])
    max_hops = args["--hops"]
    if max_hops == "num_nodes":
        max_hops = nodes
    else:
        max_hops = int(max_hops)

    edges = parse_CP(cp_file)

    reached_nodes = 0
    for current_node in range(1, nodes + 1):
        sys.stdout.write('\r')
        sys.stdout.write("{:.2f}%".format(current_node / nodes * 100))
        reached_nodes += connectivity(nodes, current_node, edges, max_hops)
        sys.stdout.flush()
    print()

    print("Reached nodes: {}".format(reached_nodes))
    print("Connectivity: {:.2f}%".format(reached_nodes / (nodes * (nodes - 1)) * 100))

if __name__ == "__main__":
    main()
