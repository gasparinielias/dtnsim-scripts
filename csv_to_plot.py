''' Given two .csv with dtnsim metrics, create a bar plot
Usage:
    csv_to_plot.py <csv>

Options:

'''
from docopt import docopt

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np
import pandas as pd
import re


def main():
    args = docopt(__doc__)
    csv = args['<csv>']

    df = pd.read_csv(csv, low_memory=False)

    #ax1 = plt.subplot(211)
    #ax2 = plt.subplot(212)
    ax3 = plt.subplot(111)

    #plot_time_to_compute_routes(df, ax1)
    #plot_table(df, ax2, 900)
    plot_per_received_messages_vs_contacts(df, ax3)

    plt.tight_layout()
    plt.show()


def plot_per_received_messages_vs_contacts(df, ax):
    received_bundles = df[
        (df['name'] == 'appBundleReceived:count') &
        (df['type'] == 'scalar') &
        (df['module'] == 'dtnsim.node[3].app')
    ]
    received_bundles = received_bundles[['run', 'value']].rename(columns={'value': 'received_bundles'})

    cp_file = df[
        (df['type'] == 'itervar') &
        (df['attrname'] == 'cpf')
    ]
    cp_file = cp_file[['run', 'attrvalue']].rename(columns={'attrvalue': 'file_name'})
    cp_file.loc[:, 'file_name'] = cp_file.loc[:, 'file_name'].apply(
        lambda file_name: re.sub(r'\.cp\.\d*$', '', file_name.replace('"', '')))

    received_bundles = pd.merge(left=received_bundles, right=cp_file, left_on='run', right_on='run')
    received_bundles = received_bundles.groupby(by='file_name').apply(
        lambda x: float('%.2f'%(x['received_bundles'].sum() / len(x.index))))

    new_index = list(map(lambda ind: int(re.sub(r'.*c(\d*).*', '\\1', ind)), received_bundles.index))
    received_bundles.index = new_index
    received_bundles.sort_index(inplace=True)

    received_bundles.plot(kind='bar', ax=ax, zorder=3)

    ax.grid(axis='y')
    ax.set_ylim(bottom=0, top=1)
    ax.set_yticks([i / 10 for i in range(11)])
    ax.yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
    ax.set_xlabel('#Contacts')
    ax.set_ylabel('% messages received')


def plot_table(df, ax, contacts_filter_value):
    routes_length = df[
        (df['type'] == 'scalar') &
        (df['name'] == 'routeCgrRouteLength:sum')
    ]
    routes_length = routes_length.groupby("run").sum().reset_index()
    routes_length = routes_length.rename(columns={'value': 'Routes length'})
    routes_length = routes_length[['run', 'Routes length']]

    length_mean = df[
        (df['type'] == 'scalar') &
        (df['name'] == 'routeCgrRouteLength:mean')
    ]
    length_mean = length_mean.groupby("run").mean().reset_index()
    length_mean = length_mean.rename(columns={'value': 'Routes length mean'})
    length_mean = length_mean[['run', 'Routes length mean']]
    length_mean.loc[:,'Routes length mean'] = length_mean['Routes length mean'].round(2)

    num_routes = df[
        (df['type'] == 'scalar') &
        (df['name'] == 'routeCgrDijkstraCalls:sum')
    ]
    num_routes = num_routes.groupby("run").sum().reset_index()
    num_routes = num_routes.rename(columns={'value': 'Number of routes'})
    num_routes = num_routes[['run', 'Number of routes']]

    processed_contacts = get_processed_contacts(df, filter_value=contacts_filter_value)
    routing_type = get_routing_type(df)
    routing_opts = get_routing_opts(df)

    table = pd.merge(left=routes_length, right=processed_contacts, left_on='run', right_on='run')
    table = pd.merge(left=table, right=num_routes, left_on='run', right_on='run')
    table = pd.merge(left=table, right=routing_type, left_on='run', right_on='run')
    table = pd.merge(left=table, right=routing_opts, left_on='run', right_on='run')
    table = pd.merge(left=table, right=length_mean, left_on='run', right_on='run')

    table = table[['Routing', 'Opts', 'Number of routes', 'Routes length', 'Routes length mean']]
    table.sort_values(by=['Routing', 'Opts'], inplace=True)
    table = ax.table(cellText=table.values, colLabels=table.columns.values, loc='center')
    table.auto_set_font_size(False)
    table.set_fontsize(14)
    ax.axis('off')


def plot_time_to_compute_routes(df, ax):
    ax.set_title('Time to compute routes vs. number of processed contacts')
    ax.set_xlabel('Contacts')
    ax.set_ylabel('Time spent (s)')

    time_to_compute_routes = df[
        (df["name"] == "routeCgrTimeToComputeRoutes:sum") &
        (df["type"] == "scalar")
    ]
    time_to_compute_routes = time_to_compute_routes.groupby("run").sum().reset_index()
    time_to_compute_routes = time_to_compute_routes.rename(columns={'value': 'SpentTime'})

    processed_contacts = get_processed_contacts(df)
    routing_type = get_routing_type(df)
    routing_opts = get_routing_opts(df)

    time_per_contact = pd.merge(left=processed_contacts, right=time_to_compute_routes, left_on='run', right_on='run')
    time_per_contact = pd.merge(left=time_per_contact, right=routing_type, left_on='run', right_on='run')
    time_per_contact = pd.merge(left=time_per_contact, right=routing_opts, left_on='run', right_on='run').reset_index()

    time_per_contact.set_index('Contacts', inplace=True)
    time_per_contact = time_per_contact.sort_index()
    time_per_contact.groupby(['Routing', 'Opts'])['SpentTime'].plot(ax=ax)

    ax.legend()


def get_processed_contacts(df, filter_value=None):
    processed_contacts = df[df['attrname'] == 'Contacts'][['run', 'attrvalue']]
    processed_contacts = processed_contacts.rename(columns={'attrvalue': 'Contacts'})
    processed_contacts = processed_contacts.astype({'Contacts': 'int32'})
    if filter_value is not None:
        return processed_contacts[processed_contacts['Contacts'] == filter_value]
    return processed_contacts


def get_routing_type(df):
    routing_type = df[df['attrname'] == 'Routing'][['run', 'attrvalue']]
    routing_type = routing_type.rename(columns={'attrvalue': 'Routing'})
    routing_type.loc[:, 'Routing'] = routing_type['Routing'].apply(lambda routing_string: routing_string.replace('"', ''))
    return routing_type


def get_routing_opts(df):
    routing_opts = df[df['attrname'] == 'Type']
    routing_opts.loc[:,'attrvalue'] = routing_opts['attrvalue'].apply(lambda x: [eb for eb in x.split(',') if 'extensionBlock' in eb][0])
    routing_opts = routing_opts[['run', 'attrvalue']].rename(columns={'attrvalue': 'Opts'})
    return routing_opts


if __name__ == '__main__':
    main()
