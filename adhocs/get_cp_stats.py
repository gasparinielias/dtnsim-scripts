'''
Usage:
    get_cp_stats.py <cp_file> <mcc> [options]

Options:
    -C --cg-edges           Compute number of contact graph edges (takes some time)
    -L --edges-per-level <levels>   Comma-dash-separated levels: get their number of edges. Ex: 4-8,1-4,8-10
'''

from docopt import docopt
import sys

def main():
    args = docopt(__doc__)
    cpfile = args['<cp_file>']
    mcc = int(args['<mcc>'])
    print_cg_edges = args['--cg-edges']
    try:
        levels = args['--edges-per-level'].split(',')
        levels = list(map(lambda l: list(map(int, l.split('-'))), levels))
        for l in levels:
            if len(l) != 2 or l[0] > l[1]:
                raise
    except:
        levels = None

    contacts = parse_contact_plan_file(cpfile, mcc)

    print('Number of contacts: {}'.format(len(contacts)))
    print('Mean contact duration: {}'.format(get_mean_contact_duration(contacts)))
    print('Max contact end time: {}'.format(get_max_contact_end_time(contacts)))
    print('Mean contact data rate: {}'.format(get_mean_contact_data_rage(contacts)))
    if print_cg_edges:
        print('Contact graph edges: {}'.format(get_cg_num_of_edges(contacts)))

    if levels is not None:
        print('Edges per level:')
        for level, num_edges in get_epl(contacts, levels).items():
            print(level, num_edges)


def parse_contact_plan_file(cpfile, mcc):
    with open(cpfile, 'r') as fp:
        lines = fp.readlines()

    contacts = []
    for l in lines:
        if 'contact' not in l or l.startswith('#'):
            continue

        _, _, start, end, f, t, r = l.split()
        f = int(f)
        t = int(t)
        r = int(r)
        start = float(start)
        end = float(end)

        if t == mcc:
            continue

        contacts.append({
            'start': start,
            'end': end,
            'from': f,
            'to': t,
            'rate': r
        })

    return contacts


def get_mean_contact_duration(contacts):
    result = 0
    for c in contacts:
        result += c['end'] - c['start']

    return result / len(contacts)


def get_max_contact_end_time(contacts):
    result = -1
    for c in contacts:
        result = max(result, c['end'])

    return result


def get_mean_contact_data_rage(contacts):
    result = 0
    for c in contacts:
        result += c['rate']

    return result / len(contacts)


def get_cg_num_of_edges(contacts):
    result = 0
    for c1 in contacts:
        for c2 in contacts:
            if c1['to'] == c2['from'] and c1['start'] < c2['end']:
                result += 1

    return result


def get_epl(contacts, levels):
    epl = {a: 0 for a in range(len(levels) - 1)}
    for c in contacts:
        level_to = find_level(c['to'], levels)
        level_from = find_level(c['from'], levels)
        if level_to != -1 and level_from != -1:
            epl[min(level_from, level_to)] += 1

    return epl

def find_level(node, levels):
    level = -1
    for i, (l, h) in enumerate(levels):
        if node >= l and node < h:
            level = i
            break
    return level


if __name__ == '__main__':
    main()
