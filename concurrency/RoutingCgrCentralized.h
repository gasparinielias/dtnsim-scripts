#ifndef SRC_NODE_DTN_ROUTINGCGRCENTRALIZED_H_
#define SRC_NODE_DTN_ROUTINGCGRCENTRALIZED_H_

#include <list>
#include <queue>
#include <limits>
#include <algorithm>
#include "CgrRoute.h"
#include "ContactPlan.h"

class RoutingCgrCentralized
{
public:
    RoutingCgrCentralized(int eid, int neighborsNum, ContactPlan *localContactPlan, bool printDebug, string routingType, int maxRouteLength, int maxRoutesWithSameDst);
    virtual ~RoutingCgrCentralized();
    void initializeRouteTable();

    void fillRouteTableWithContactFilter(bool comparisonFunc (const Contact*, const Contact*));
    CgrRoute findBestRoute(int terminusNode);

    // stats
    int getComputedRoutes();
    vector<int> getRouteLengthVector();
    double getTimeToComputeRoutes();
    void clearRouteLengthVector();

private:
    void findNextBestRoute(vector<int> suppressedContactIds, int terminusNode, CgrRoute * route);

    void createContactsWork();
    void initializeContactsWork();
    void resetContactsWork();
    void clearContactsWork();

    // stats
    int computedRoutes_;
    double timeToComputeRoutes_;
    vector<int> routeLengthVector_;

    int eid_;
    ContactPlan* contactPlan_;
    bool printDebug_;
    int neighborsNum_;
    string routingType_;
    vector<vector<CgrRoute>> routeTable_;
    double simTime_;
    int maxRouteHops_;
    int maxRoutesWithSameDst_;
    double bfsTimeInterval_;

    typedef struct {
        Contact * predecessor;      // Predecessor Contact
        double arrivalTime;         // Dijkstra exploration: best arrival time so far
        bool visited;               // Dijkstra exploration: visited
        bool suppressed;            // Dijkstra exploration: suppressed

        bool operator()(Contact const *a, Contact const *b) {
            return ((Work *) a->work)->arrivalTime > ((Work *) b->work)->arrivalTime;
        }
    } Work;
};

#endif
