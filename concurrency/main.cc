#include <iostream>
#include <thread>
#include "ContactPlan.h"
#include "RoutingCgrCentralized.h"

using namespace std;

void th(int eid, int nodesNum, ContactPlan* cp, double* compute_t, int* numRoutes);

int main() {
    // Crear contact plan
    string cpFileName = "./cps/walker-12leo-10gs-100gt-24hrs-contact-plan.txt";
    int nodesNum = 123;

    // Por cada nodo, crear threads ejecutando FE
    double tarr[nodesNum + 1];
    int croutes[nodesNum + 1];
    thread tharr[nodesNum + 1];
    ContactPlan cps[nodesNum + 1];

    int toP = 122;
    for (int i = 1; i <= toP; i++) {
        cps[i].parseContactPlanFile(cpFileName, nodesNum, -1);
        tharr[i] = thread(th, i, nodesNum, &cps[i], &tarr[i], &croutes[i]);
        //th(i, nodesNum, &cps[i], &tarr[i], &croutes[i]);
    }

    double tsum = 0.0;
    int c = 0;
    for (int i = 1; i <= toP; i++) {
        tharr[i].join();
        tsum += tarr[i];
        c += croutes[i];
    }
    cout << "Time spent: " << tsum << endl;
    cout << "Computed routes: " << c << endl;
}

void th(int eid, int nodesNum, ContactPlan* cp, double* compute_t, int* numRoutes) {
    string routingType("routeListType:firstEnded,volumeAware:allContacts,extensionBlock:off,contactPlan:local");
    RoutingCgrCentralized routing = RoutingCgrCentralized(eid, nodesNum, cp, true, routingType, -1, -1);
    *compute_t = routing.getTimeToComputeRoutes();
    *numRoutes = routing.getComputedRoutes();
}
