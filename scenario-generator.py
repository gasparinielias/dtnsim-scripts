'''
Usage:
    scenerio_generator.py <num_contacts> <max_scenario_duration> (-l <nodes_per_level> ...) [options]

Options:
    -o --out <out_file_name>            Output file [default: out.cp]
    -d --dir <out_dir>                  Output directory [default: .]
    -m --mean <mean_contact_duration>   Mean contact duration [default: 1]
    -r --rate <transfer_rate>           Contact's capacity per second [default: 1]
    -b --batch <batch_size>             Generate <batch_size> contact plans. 
    -M --msc                            From level one create permanent links to a new node.
    -F --full-duplex                    Each contact will be doubled in the other direction.
    -h --help                           Show this screen.

'''
from docopt import docopt

import numpy as np
import os
import random

from collections import defaultdict


def main():
    args = docopt(__doc__)
    num_contacts = int(args['<num_contacts>'])
    nodes_per_level = args['<nodes_per_level>']
    max_scenario_duration = int(args['<max_scenario_duration>'])
    mean_contact_duration = int(args['--mean'])
    rate = int(args['--rate'])
    out_file_name = args['--out']
    out_dir_name = args['--dir']
    batch = args['--batch']
    msc = args['--msc']
    full_duplex = args['--full-duplex']

    if len(nodes_per_level) < 2:
        raise Exception('Number of levels must be equal or greater than 2')

    levels = []
    n = 1
    for num_nodes in map(int, nodes_per_level):
        levels.append({'from': n, 'to': n + num_nodes})
        n += num_nodes
    total_nodes = n - 1

    permanent_links = defaultdict(lambda: defaultdict(list))
    if msc:
        total_nodes += 1
        msc = total_nodes
        for i in range(levels[0]['from'], levels[0]['to']):
            permanent_links[i][msc] = [(0, max_scenario_duration)]

    if full_duplex:
        num_contacts //= 2

    if batch is None:
        produce_contact_plan(levels, num_contacts, max_scenario_duration, mean_contact_duration,
            total_nodes, full_duplex, out_dir_name, out_file_name, rate, permanent_links)

    else:
        for i in range(int(batch)):
            produce_contact_plan(levels, num_contacts, max_scenario_duration, mean_contact_duration,
                total_nodes, full_duplex, out_dir_name, '{}.{}'.format(out_file_name, i), rate, permanent_links)


def produce_contact_plan(levels, num_contacts, max_scenario_duration, mean_contact_duration,
                         total_nodes, full_duplex, out_dir_name, out_file_name, rate, permanent_links):
    contacts = generate_contacts(
        levels, num_contacts, max_scenario_duration,
        mean_contact_duration, full_duplex
        )

    for node, links in permanent_links.items():
        contacts[node].update(links)

    print_contact_plan(total_nodes, max_scenario_duration, contacts,
        out_dir_name, out_file_name, rate)

def generate_contacts(levels, num_contacts, max_scenario_duration,
        mean_contact_duration, full_duplex):

    contacts = defaultdict(lambda: defaultdict(list))
    intervals = defaultdict(lambda: defaultdict(list))
    node_pairs = []
    for level1, level2 in zip(levels[:-1], levels[1:]):
        node_pairs += [pair
            for x in range(level1['from'], level1['to'])
            for y in range(level2['from'], level2['to'])
            for pair in ((x, y), (y, x))
        ]

    for i in range(num_contacts):
        # Select two random nodes
        try:
            x, y = random.sample(node_pairs, 1)[0]
        except ValueError:
            print('No more selectable nodes. Stopping at {} contacts'.format(i))
            break

        # Find a valid contact duration that fits in an available interval
        available_intervals = intervals[x][y]
        if len(contacts[x][y]) == 0 and len(available_intervals) == 0:
            available_intervals.append((0, max_scenario_duration))

        if full_duplex and len(contacts[y][x]) == 0 and len(intervals[y][x]) == 0:
            intervals[y][x].append((0, max_scenario_duration))

        interval = None
        contact_duration = 0
        while interval is None:
            contact_duration = max(1, round(np.random.exponential(mean_contact_duration)))
            interval = find_interval_to_fit_duration(available_intervals, contact_duration)

        start = interval[0]
        end = interval[1]
        contact_start_time = random.randint(start, end - contact_duration)
        contact_end_time = contact_start_time + contact_duration

        # Add new contacts
        contacts[x][y].append((contact_start_time, contact_end_time))

        # Update available intervals
        update_intervals(available_intervals, interval, contact_start_time, contact_end_time)

        if len(available_intervals) == 0:
            node_pairs.remove((x, y))
            if full_duplex:
                node_pairs.remove((y, x))

        if full_duplex:
            duplex_interval = find_interval_to_fit_start_end(
                intervals[y][x], contact_start_time, contact_end_time
            )
            assert(duplex_interval is not None)
            contacts[y][x].append((contact_start_time, contact_end_time))
            update_intervals(intervals[y][x], duplex_interval, contact_start_time, contact_end_time)


    return contacts


def update_intervals(available_intervals, interval, contact_start_time, contact_end_time):
        start = interval[0]
        end = interval[1]
        available_intervals.remove(interval)

        if contact_start_time - start > 1:
            available_intervals.append((start, contact_start_time - 1))

        if end - contact_end_time > 1:
            available_intervals.append((contact_end_time + 1, end))


def find_interval_to_fit_duration(intervals, duration):
    result = None
    for interval in intervals:
        start = interval[0]
        end = interval[1]
        if end - start >= duration:
            result = interval
            break

    return result


def find_interval_to_fit_start_end(intervals, contact_start_time, contact_end_time):
    result = None
    for interval in intervals:
        start = interval[0]
        end = interval[1]
        if start <= contact_start_time and contact_end_time <= end:
            result = interval
            break

    return result


def print_contact_plan(num_nodes, sim_time, contacts, out_dir, out_file_name, transfer_rate):
    try:
        f = open(os.path.join(out_dir, out_file_name), 'w')
    except FileNotFoundError:
        print('Missing directory: {}'.format(out_dir))
        exit(1)

    f.write('# Num nodes: {}\n# Sim time: {}\n\n'.format(num_nodes, sim_time))
    for x in contacts.keys():
        for y in contacts[x].keys():
            for contact in contacts[x][y]:
                f.write('a contact {} {} {} {} {}\n'.format(
                    contact[0], contact[1], x, y, transfer_rate))
                f.write('a range {} {} {} {} 0\n'.format(
                    contact[0], contact[1], x, y))

    f.close()


if __name__ == '__main__':
    random.seed(54321)
    np.random.seed(54321)
    main()
