'''
Given a .ini omnet template, generate only telemetry traffic. If multiple destinations are given,
a random one will be selected for each packet.

Usage:
    generate_telemetry_ini.py <template_file> <num_nodes> (<destination_nodes> ...) [options]

Options:
    -L --low <time>                 Low-traffic generation interval
    -M --medium <time>              Medium-traffic generation interval
    -H --high <time>                High-traffic generation interval
    -o --output <file_name>         Output file [default: out.ini]
    -p --packet-size <size>         Telemetry packet size in bytes [default: 100]
    -s --sim-time-limit <time>      Simulation time limit in seconds [default: 300]
    -h --help                       Print this screen
'''

from collections import defaultdict
import configparser
from docopt import docopt
import numpy as np



def main(template_file, num_nodes, destination_nodes, intervals,
         packet_size, sim_time_limit, output_file):

    config = configparser.ConfigParser(allow_no_value=True)
    config.optionxform = str
    config.read(template_file)

    packet_times = generate_packet_times(intervals, sim_time_limit)
    nodes_to_config = get_nodes_to_config(num_nodes, destination_nodes)

    config['General']['sim-time-limit'] = str(sim_time_limit) + 's'
    config.set('General', 'dtnsim.nodesNumber', '${{N={}}}'.format(num_nodes))
    config['General']['dtnsim.node[1..{}].icon'.format(num_nodes)] = '"satellite"'

    if 'Bfs' in config.keys():
        config['Bfs']['dtnsim.node[*].dtn.bfsIntervalNum'] = str(sim_time_limit) + s

    for traffic_type, interval in intervals.items():
        config.set(traffic_type, '# Mean generation interval: {}'.format(interval))

        num_packets = len(packet_times[traffic_type])
        config[traffic_type][nodes_to_config + '.app.enable'] = 'true'

        config[traffic_type][nodes_to_config + '.app.bundlesNumber'] = \
            '"' + ','.join(['1'] * num_packets) + '"'

        config[traffic_type][nodes_to_config + '.app.start'] = \
            '"' + ','.join(map(str, (packet_times[traffic_type]))) + '"'

        config[traffic_type][nodes_to_config + '.app.destinationEid'] = \
            '"' + ','.join(map(str, np.random.choice(destination_nodes, num_packets))) + '"'

        config[traffic_type][nodes_to_config + '.app.size'] = \
            '"' + ','.join([str(packet_size)] * num_packets) + '"'

    with open(output_file, 'w+') as fp:
        config.write(fp)


def generate_packet_times(intervals, sim_time_limit):
    result = defaultdict(list)
    for traffic_type, time in intervals.items():
        packet_time = 0
        while packet_time < sim_time_limit:
            result[traffic_type].append(packet_time)
            packet_time += time

    return result


def get_nodes_to_config(num_nodes, destination_nodes):
    prefix = 'dtnsim.node['
    sufix = ']'
    i = 1
    node_ranges = []
    while i in destination_nodes:
        i += 1
    n1 = i
    while i <= num_nodes:
        while i not in destination_nodes and i <= num_nodes:
            i += 1
        node_ranges.append((n1, i - 1))
        while i in destination_nodes:
            i += 1
        n1 = i
    nodes = ','.join([str(n1) + '..' + str(n2) if n1 != n2 else str(n1) for n1, n2 in node_ranges ])

    return prefix + nodes + sufix


if __name__ == '__main__':
    args = docopt(__doc__)
    template_file = args['<template_file>']
    num_nodes = int(args['<num_nodes>'])
    destination_nodes = list(map(int, args['<destination_nodes>']))
    intervals = {
        'Config TrafficLow': float(args['--low']),
        'Config TrafficMedium': float(args['--medium']),
        'Config TrafficHigh': float(args['--high']),
        'Config Centralized': float(args['--high'])
    }
    sim_time_limit = int(args['--sim-time-limit'])
    packet_size = int(args['--packet-size'])
    output_file = args['--output']

    np.random.seed(54321)

    main(template_file, num_nodes, destination_nodes, intervals,
         packet_size, sim_time_limit, output_file)
