USAGE=$(cat <<EOF
Usage:
    run_simulations.sh <path/to/file.ini> <output/path/file.csv>
EOF
)

DTNSIM_DIR="/home/mono/materias/posible-tesis/dtnsim/dtnsim";
OPP_RUN="$DTNSIM_DIR/out/gcc-release/dtnsim";
SRC_DIR="$DTNSIM_DIR/src";
OLD_DIR=$PWD;

### Check parameters
if [ $# -lt 2 ]; then echo "$USAGE"; exit 1; fi

INI_FILE="$DTNSIM_DIR/$1";
RUN_DIR=`dirname $INI_FILE`;
echo $RUN_DIR;
if [ ! -f $INI_FILE ]; then echo "Not such .ini file"; exit 1; fi
OUT_CSV=$2;

if [ -f $OUT_CSV ]; then
    echo "File $OUT_CSV already exists. Overwrite it? y/[n]";
    read ans;
    if [ "$ans" == "y" ]; then
        rm $OUT_CSV;
    else
        echo "Aborting...";
        exit 1;
    fi
else
    mkdir -p $OUT_CSV;
    rm -r $OUT_CSV;
fi

### Run omnet++ simulation
cd $RUN_DIR
rm -r results;
$OPP_RUN -f $INI_FILE -n $SRC_DIR -u Cmdenv

### Export .csv
echo "Exporting results..."
cd $OLD_DIR;
scavetool x "$RUN_DIR/results/"{*.sca,*.vec} -o $OUT_CSV -F CSV-R;
echo "";
echo "Exported file $OUT_CSV";
